<?php

/**
 *
 */
class dbh
{

  private $servername;
  private $username;
  private $password;
  private $dbname;
  private $charset;  //for(PDO)

  protected function connect()
  {
    $this->servername = "localhost";
    $this->username = "root";
    $this->password = "";
    $this->dbname = "db_2_xirpl_si_perpustakaan_abdul";
    $this->charset = "utf8mb4";
    // !with() PDO
    // $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
    // return $conn;
    //
    try {
      // with() PDO
      $dsn = "mysql:host=".$this->servername.";dbname=".$this->dbname.";charset=".$this->charset;
      $pdo = new PDO($dsn, $this->username, $this->password);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      return $pdo;
    } catch (PDOException $e) {
        echo "Connection Failed";
    }
  }
}
