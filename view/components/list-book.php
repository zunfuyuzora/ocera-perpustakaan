<div class="container" id="booklist">
    <div class="list">
        <h2>List Of Book</h2>
            <table>
                <tr>
                    <th>No</th>
                    <th>Book Title</th>
                    <th>Genre</th>
                </tr>
            </table>
        <div class="list-of-book">
            <table>
                <?php
                for($i = 1; $i <= 20; $i++) : ?>
                <tr>
                    <td><?= $i; ?></td>
                    <td>:Book-Title</td>
                    <td>:Genre</td>
                </tr>
    <?php endfor; ?>
            </table>
        </div> 
    </div>
    <div class="info" id="book-details">
        <div class="cover">
            <img src="assets/img/iroduku.jpg" alt="">
        </div>
        <div class="details">
            <h1>:Title</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum incidunt eius rerum dolorum voluptatum quisquam quis voluptatem modi accusamus assumenda possimus, cum libero quibusdam quae magni at? Deleniti, rerum! Qui?</p>
        </div>
    </div>
</div>